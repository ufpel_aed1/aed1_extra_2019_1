# AED1 - Trabalho Extra - Árvore AVL

Declaro que o presente trabalho contém código desenvolvido exclusivamente por mim e que não foi compartilhado de nenhuma forma com terceiros a não ser o professor da disciplina. Compreendo que qualquer violação destes princípios será punida rigorosamente de acordo com o Regimento da UFPEL.

(Preencha com seus dados)

- Nome completo: 
- Username do Bitbucket: 
- Email @inf: 


## Descrição 

Este trabalho consiste no desenvolvimento de código para uma árvore binária de pesquisa AVL que armazena valores inteiros. As funções a serem implementadas estão declaradas em *arvore.h*. 

A perfeita execução dos testes libera o aluno das questões sobre árvores AVL na prova 2 (recebendo a respectiva nota).


## Produtos

- arvore.c: implementação das funções da árvore

A versão final deverá executar os testes dos Professores no Bitbucket usando a regra _make_. 

O Makefile poderá ser modificado para adicionar as regras para construir as bibliotecas de listas, filas e pilhas que serão copiadas do repositório do aluno. 



## Cronograma

- Entrega final: 10/06/2019


